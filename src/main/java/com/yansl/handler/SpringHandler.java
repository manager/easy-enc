package com.yansl.http.handler;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @description: 配置参数获取
 * @author: yansl.cn
 * @create: 2022-12-17 17:28
 */
@Component
public class SpringHandler implements EnvironmentAware, ApplicationContextAware {
    public static Environment environment;
    private static ApplicationContext applicationContext;

    @Override
    public void setEnvironment(Environment environment) {
        SpringHandler.environment = environment;
    }

    /**
     * 获取容器
     * 
     * @param applicationContext
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringHandler.applicationContext = applicationContext;
    }

    /**
     * 获取配置参数
     *
     * @param key
     * @return
     */
    public static String getProperties(String key) {
        return environment.getProperty(key);
    }

    public static Object getByType(Class clazz) {
        return applicationContext.getBean(clazz);
    }

}
