package com.yansl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @description:
 * @author: eshengtai
 * @create: 2022-12-17 10:00
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class EncStater {

    public static void main(String[] args) {
        SpringApplication.run(EncStater.class);
    }
}
