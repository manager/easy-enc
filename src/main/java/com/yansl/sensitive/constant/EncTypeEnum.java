package com.yansl.sensitive.constant;

/**
 * @description: 加密类型
 * @author: yansl.cn
 * @create: 2023-01-10 13:11
 */
public enum EncTypeEnum {

    SYS_ENC(0, "系统加密"), CUSTOM_ENC(1, "自定义加密");

    private Integer code;
    private String name;

    EncTypeEnum(final Integer code, final String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(final Integer code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
