package com.yansl.sensitive.core;

import com.fasterxml.jackson.databind.JavaType;
import com.yansl.sensitive.annotation.Enc;

/**
 * @description: 自定义处理类
 * @author: yansl.cn
 * @create: 2023-01-10 13:28
 */
public interface IEncDefaultHandler {

    /**
     * 转换数据
     * 
     * @param annotation
     *            属性注解
     * @param fieldType
     *            属性类型
     * @param fieldName
     *            属性名称
     * @param val
     *            属性值
     * @return
     */
    default String transfor(Enc annotation, JavaType fieldType, String fieldName, String val) {
        return val;
    };
}
