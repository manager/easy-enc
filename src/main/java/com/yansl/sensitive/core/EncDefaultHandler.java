package com.yansl.sensitive.core;

import com.fasterxml.jackson.databind.JavaType;
import com.yansl.sensitive.annotation.Enc;

import lombok.extern.slf4j.Slf4j;

/**
 * @description: 自定义处理类
 * @author: yansl.cn
 * @create: 2023-01-10 13:28
 */
@Slf4j
public class EncDefaultHandler implements IEncDefaultHandler {
    @Override
    public String transfor(Enc annotation, JavaType fieldType, String fieldName, String val) {
        log.info("自定义加密，请实现【IEncDefaultHandler】接口");
        return val;
    }
}
