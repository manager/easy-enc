package com.yansl.sensitive.model;

import com.yansl.sensitive.annotation.Enc;
import com.yansl.sensitive.constant.EncTypeEnum;

import lombok.Data;

/**
 * @description:
 * @author: yansl.cn
 * @create: 2023-01-10 11:07
 */
@Data
public class Demo {
    private String name2;
    @Enc
    private String name1;
    @Enc(before = 2)
    private String name;

    @Enc(after = 2)
    private String phone;

    @Enc(before = 2, after = 2, symbol = '?')
    private String remark;

    @Enc(type = EncTypeEnum.CUSTOM_ENC)
    private String desc;
}
