package com.yansl.sensitive.annotation;

import java.lang.annotation.*;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yansl.sensitive.constant.EncTypeEnum;
import com.yansl.sensitive.core.EncCoreHandler;

/**
 * @description: 加密属性，脱敏
 * @author: yansl.cn
 * @create: 2023-01-10 11:57
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@JacksonAnnotationsInside
@JsonSerialize(using = EncCoreHandler.class)
public @interface Enc {

    /**
     * 默认自定义加密，回调处理
     * 
     * @return
     */
    EncTypeEnum type() default EncTypeEnum.SYS_ENC;

    /**
     * 前缀空余个数：before=3 123****，abc****
     * 
     * @return
     */
    int before() default 0;

    /**
     * 后缀空余个数：after=3 ****123，****abc
     *
     * @return
     */
    int after() default 0;

    /**
     * 加密符号
     * 
     * @return
     */
    char symbol() default '*';
}
