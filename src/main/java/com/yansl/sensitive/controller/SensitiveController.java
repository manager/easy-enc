package com.yansl.sensitive.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yansl.sensitive.model.ABC;
import com.yansl.sensitive.model.Demo;

/**
 * @description: API 脱敏
 * @author: yansl.cn
 * @create: 2023-01-10 11:07
 */
@RestController
@RequestMapping("enc")
public class SensitiveController {

    @RequestMapping("demo")
    public Object demo() {
        Demo demo = new Demo();
        demo.setName1("zhangshan1");
        demo.setName("zhangshan");
        demo.setPhone("14782423919");
        demo.setDesc("wodeaihaoguangfan");
        demo.setRemark("nimenshuoshibushihenghaodejieguo");
        demo.setName2("wodshifsefse");
        return demo;
    }

    @RequestMapping("abc")
    public Object abc() {
        return new ABC("abc1", "1234");
    }

}
