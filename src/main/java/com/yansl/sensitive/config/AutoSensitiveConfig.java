package com.yansl.sensitive.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.yansl.sensitive.core.EncDefaultHandler;
import com.yansl.sensitive.core.IEncDefaultHandler;

/**
 * @description: 自动化配置
 * @author: yansl.cn
 * @create: 2023-01-10 14:44
 */
@Configuration
public class AutoSensitiveConfig {
    @Bean
    @ConditionalOnMissingBean(IEncDefaultHandler.class)
    public EncDefaultHandler encDefaultHandler() {
        return new EncDefaultHandler();
    }

}
